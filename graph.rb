#encoding: utf-8

require 'gruff'

def main_get_gruff xmin, xmax, n, title='Graph of the dead', resolution='1200x800'
  g = Gruff::Bezier.new(resolution.to_s)
  g.title = title
  g.y_axis_label = "pH"
  g.x_axis_label = "Volume (mL)"

  xmax = xmax.to_i
  xmin = xmin.to_i

  xmax_ = (xmax - xmin) * (n / 2)
  xmin_ = 0

  g.labels = {
    xmin_ => xmin.to_s,
    (xmax_ + xmin) / 2  => '0',
    (xmax_ + xmin_) - 1 => xmax.to_s,
  }
  return g
end
