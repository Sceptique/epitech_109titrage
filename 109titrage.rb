#!/usr/bin/env ruby
#encoding: utf-8

require 'epimath100'
require_relative 'values'
require_relative 'graph'

# ==taux avant +h (taux en x1)
# x1 = c  
# x2 = c + h  
def calc_taux_avant x1, x2, y1, y2
  (y2 - y1) / (x2 - x1)
end

# ==taux arrière -h (taux en x2)
# x1 = c - h  
# x2 = c  
def calc_taux_arrière x1, x2, y1, y2
  (y2 - y1) / (x2 - x1)
end

# ==taux centré en +h/2, -h2/ (taux en c ? lol)
# x1 = c - h/2
# x2 = c + h/2
def calc_taux_centré x1, x2, y1, y2
  # on l'utilise pas, la fleme, parce que h² > h dans notre cas
end

def calc_derive data
  derive = []
  (data.size - 2).times do |i|
    x = [data[i][0], data[i+1][0]]
    y = [data[i][1], data[i+1][1]]
    if x[0] == x[1]
      derive << 0
    else
      derive << calc_taux_avant(x[0], x[1], y[0], y[1])
      derive[-1] = 0 if derive[-1] < 0
    end
  end
  return derive
end

def get_line(data, coef, ordo)
  out = []
  data.size.times do |i|
    i = coef * 0.1 * i + ordo
    i = 0 if i < 0
    i = 14 if i > 14
    out << i
  end
  return out
end

def get_options a
  options = {}
  i = 0

  options[:resolution] = "1200x900"
  options[:titre] = "Volume équivalent"
  options[:mod] = 1
  options[:angle] = 45.0
  options[:output] = "png"
  options[:display] = true

  while i < a.size do

    if a[i].match /(\-t)|(\-\-title)/ and a[i+1].to_s.size > 1
      i += 1
      options[:titre] = a[i]

    elsif a[i].match /(\-r)|(\-\-resolution)/ and a[i+1].match /\d+x\d+/
      i += 1
      options[:resolution] = a[i]

    elsif a[i].match /(\-o)|(\-\-out)|(\-\-output)/ and a[i+1].match /(jpg)|(png)|(pdf)/
      i += 1
      options[:output] = a[i]

    elsif a[i].match /(\-\-nodisplay)/
      options[:display] = false

    elsif a[i].match /\d+(\.\d+)?/
      options[:mod] = 2
      options[:angle] = a[i].to_f % 360 + 360
    end

    i += 1
  end

  return options
end

#  max = derive.max
#  id = derive.index(max)
#  ve = map[id]
#  map.size.times {|i| puts i.to_s + "\t" + map[i].to_s + "\t" + derive[i].to_s + "\t" + data[i][0].to_s}
#  puts max
#  puts id
#  puts ve

# tester la pente au point data[i] si elle est la même que ANGLE
# (même = ANGLE est entre le précedent et le suivant)
# g(x) = f'(a) (x - a) + f(a)
# g(x) = f'(a)*x -a*f'(a) + f(a)
# soit un coef directeur de f'(a)
# soit h(x) = f'(a) * x
# h(0) = 0 ; h(1) = f'(a)
# donc angle = atan(1 / h(1))
def main argv
  options = get_options ARGV

  data_init = get_values()
  data = data_init
  data = get_regular_values(data)
  data = convert_regular_data(data)

  map = data.map{|d| d[1]}[0..-1]
  derive = calc_derive(data)[0..-1]
  data = data[0..-1]

  id = derive.index(derive.max)
  puts "Volume Equivalent : #{data[id][0]} mL"

  g = main_get_gruff 0, data.size / 10, data.size / 10, options[:titre], options[:resolution]
  g.data :ph_metrie, map
  g.data :derivé, derive if options[:mod] == 1

  if options[:mod] == 2
    angle = (options[:angle] % 90) * Math::PI / 180
    coef = Math.tan(angle)
    j = 0

#    t_possibs = []
#    (0..(map.size)).each do |i|
#      break if derive[i] == nil
#      a_curr = Math.atan2(derive[i], 1)
#      if a_curr >= angle
#        t_possibs << i
#      end
#    end

    (0..(map.size)).each do |i|
      a_curr = Math.atan2(derive[i], 1)
      j = i
      break if a_curr >= angle
    end
#   j = t_possibs[t_possibs.size / 2]
    ordo1 = map[j] - coef * data[j][0]
    g.data :t1, get_line(data, coef, ordo1)

    (map.size - j).downto(j).each do |i|
      a_curr = Math.atan2(derive[i], 1)
      j = i
      break if a_curr >= angle
    end
#    j = t_possibs[t_possibs.size / 2 + 1]

    ordo2 = data[j][1] - coef * data[j][0]
    g.data :t2, get_line(data, coef, ordo2)
    g.data :t3, get_line(data, coef, (ordo1 + ordo2) / 2);
  end

  file = "out." + options[:output].to_s
  g.write(file)
  `eog #{file}` if options[:output].match /(png)|(jpg)/ and options[:display] == true
end

main(ARGV)
